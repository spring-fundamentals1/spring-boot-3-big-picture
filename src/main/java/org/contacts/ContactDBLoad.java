package org.contacts;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ContactDBLoad {

    @Bean
    CommandLineRunner initDatabase(ContactRepository repository) {
        return args -> {
            System.out.println("Preloading " + repository.save(new Contact("Bhushan Jain", "23-789-6544")));
            System.out.println("Preloading " + repository.save(new Contact("Kshitija Pandit", "123-784-9874")));
        };
    }
}
