package org.contacts;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ContactController {

    @Autowired
    private ContactRepository repository;

    @GetMapping("/contacts")
    List<Contact> all() {return repository.findAll();}

    @GetMapping("")
    String index() {
        return "<a href='http://127.0.0.1:8080/contacts'>click here </a>";
    }
}
